/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Earthworm extends Reptile{
    private String name;

    public Earthworm(String name) {
        super("Earthworm", 0);
        this.name = name;
    }

    @Override
    public void crawl() {
        System.out.println("Earthworm: " + name + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Earthworm: " + name + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Earthworm: " + name + " speak");
    }

    @Override
    public void walk() {
        System.out.println("Earthworm: " + name + " walk");
    }

    @Override
    public void sleep() {
        System.out.println("Earthworm: " + name + " sleep");
    }
    
}
