/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class TestAnimal {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine Number One");
        bat.fly();// Animeal.Poultry,Flyable
        plane.fly();// Vahicle,Flyable
        Dog dog = new Dog("Dang");
        Car car = new Car("Engine Number Two");
        Cat cat = new Cat("Black");
        Bird bird = new Bird(); 
        bird.fly();
        Crab crab = new Crab("Boung");
        crab.eat();
        crab.speak();
        crab.swim();
        Earthworm eartworm = new Earthworm("Earth");
        eartworm.crawl();
        eartworm.eat();
        eartworm.sleep();
        eartworm.speak();        
        Human human = new Human("Mark");
        human.eat();
        human.sleep();
        human.speak();
        Snake snake = new Snake("Ging");
        snake.crawl();
        snake.eat();
        snake.sleep();
        snake.speak();  
        Crocodile cro = new Crocodile("Two");
        cro.crawl();
        cro.eat();
        cro.sleep();
        cro.speak();  
        
        
        Flyable[] flyable = {bird,bat,plane};
        for(Flyable f : flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runable = {dog,plane,car,cat,human};
        for(Runable r : runable){
            r.run();
        }
        
    }
   
}
