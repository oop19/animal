/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Bird extends Poultry{

    public Bird() {
        super();
    }
    
    @Override
    public void fly() {
        System.out.println("Bird: Fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird: eat");
    }

    @Override
    public void speak() {
        System.out.println("Bird: speak");
    }

    @Override
    public void walk() {
        System.out.println("Bird: walk");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: sleep");
    }
    
}
